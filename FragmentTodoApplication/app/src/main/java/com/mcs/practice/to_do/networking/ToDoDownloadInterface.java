package com.mcs.practice.to_do.networking;


import com.mcs.practice.to_do.model.ToDo;

import java.util.List;

public interface ToDoDownloadInterface {
    void onTodoResult(List<ToDo> toDoList);
    void onError(Exception exception);
}

package com.sample.fragmentsdualpane;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sample.fragmentsdualpane.model.Email;

public class EmailDetailsActivity extends AppCompatActivity {
    public static final String EMAIL_EXTRA = "email_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_details);

        Email email = (Email) getIntent().getExtras().get(EMAIL_EXTRA);

        // Initialize and load the fragment into the frame layout
        EmailDetailsFragment emailDetailsFragment =
                EmailDetailsFragment.newInstance(email);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_email_details, emailDetailsFragment);
        fragmentTransaction.commit();
    }
}

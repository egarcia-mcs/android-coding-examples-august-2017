package com.sample.fragmentsdualpane;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.fragmentsdualpane.model.Email;

import java.util.List;


public class EmailListAdapter extends RecyclerView.Adapter<EmailListAdapter.ViewHolder> {

    private List<Email> mItems;
    private AdapterView.OnItemClickListener listener;

    public EmailListAdapter(List<Email> mItems, AdapterView.OnItemClickListener listener) {
        this.mItems = mItems;
        this.listener = listener;
    }

    @Override
    public EmailListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new EmailListAdapter.ViewHolder(inflatedView, listener);
    }

    @Override
    public void onBindViewHolder(EmailListAdapter.ViewHolder holder, int position) {
        Email email = mItems.get(position);
        holder.emailHeader.setText(email.getHeader());
        holder.emailContent.setText(email.getContent());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public Email getItem(int position) {
        return mItems.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView emailImage;
        public TextView emailHeader;
        public TextView emailContent;
        public AdapterView.OnItemClickListener listener;

        public ViewHolder(View itemView, AdapterView.OnItemClickListener onItemClickListener) {
            super(itemView);
            emailImage = itemView.findViewById(R.id.email_header_image);
            emailHeader = itemView.findViewById(R.id.email_header);
            emailContent = itemView.findViewById(R.id.email_content);
            listener = onItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(null, view, getAdapterPosition(), view.getId());
        }
    }
}

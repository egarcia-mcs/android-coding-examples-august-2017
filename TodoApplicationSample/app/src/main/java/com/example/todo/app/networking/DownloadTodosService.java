package com.example.todo.app.networking;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.todo.app.model.ToDo;
import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class DownloadTodosService extends IntentService {

    public DownloadTodosService() {
        super(DownloadTodosService.class.getSimpleName());
    }

    public DownloadTodosService(String name) {
        super(name);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (NetworkUtils.isOnline(getApplicationContext()) ) {
            // Do networking
            Bundle data = intent.getExtras();
            String serverAddress = data.getString(NetworkUtils.SERVER_URL_EXTRA);
            ResultReceiver resultReceiver =
                    (ResultReceiver) data.get(
                            NetworkUtils.RESULT_RECEIVER_EXTRA);
            try {
                URL url = new URL(serverAddress);
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream)
                );


                StringBuilder stringBuilder = new StringBuilder();
                String inputString;

                while((inputString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(inputString);
                }

                JSONArray jsonArray = new JSONArray(stringBuilder.toString());
                ArrayList<ToDo> todosList = ToDo.parseJSONArray(jsonArray);
                connection.disconnect();
                // return todosList
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(NetworkUtils.TODO_LIST_EXTRAS, todosList);
                if (resultReceiver != null) {
                    resultReceiver.send(
                            TodoResultReceiver.RESULT_CODE_OK,
                            bundle);
                }

            } catch (Exception exception) {
                // Manage exception
                Log.d(this.getClass().getSimpleName(), exception.getLocalizedMessage());
                Bundle bundle = new Bundle();
                bundle.putString(NetworkUtils.RESULT_RECEIVER_ERROR_EXTRA, exception.getLocalizedMessage());
                if (resultReceiver != null) {
                    resultReceiver.send(TodoResultReceiver.RESULT_CODE_ERROR, bundle);
                }
            }
            // No data or an exception happened.
            //return null;
        } else {
            // Notify user there is no connectivity
        }
    }
}

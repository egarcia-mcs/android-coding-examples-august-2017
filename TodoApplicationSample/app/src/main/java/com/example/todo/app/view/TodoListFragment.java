package com.example.todo.app.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.todo.app.R;
import com.example.todo.app.model.ToDo;


public class TodoListFragment extends Fragment {

	private TodoListListener mListener;

	public TodoListFragment() {
		// Required empty public constructor
	}

	public static TodoListFragment newInstance() {
		TodoListFragment fragment = new TodoListFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_todo_list, container, false);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof TodoListListener) {
			mListener = (TodoListListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement TodoListListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface TodoListListener {
		void onTodoItemSelected(ToDo toDo);
	}
}

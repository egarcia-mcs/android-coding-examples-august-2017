package com.mcs.practice.to_do.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mcs.practice.to_do.R;
import com.mcs.practice.to_do.model.ToDo;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link TodoDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TodoDetailsFragment extends Fragment {
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_TODO = "ARG_TODO_ITEM";

	private ToDo mTodo;

	public TodoDetailsFragment() {
		// Required empty public constructor
	}

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param toDo the Todo object to pass as a parameter to the fragment.
	 * @return A new instance of fragment TodoDetailsFragment.
	 */
	public static TodoDetailsFragment newInstance(ToDo toDo) {
		TodoDetailsFragment fragment = new TodoDetailsFragment();
		Bundle args = new Bundle();
		args.putParcelable(ARG_TODO, toDo);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mTodo = getArguments().getParcelable(ARG_TODO);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_todo_details, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		TextView textViewHeader = view.findViewById(R.id.lbl_todo_title);
		TextView textViewDetails = view.findViewById(R.id.lbl_todo_details);

		textViewHeader.setText(mTodo.getTitle());
		textViewDetails.setText(mTodo.getDetails());
	}
}

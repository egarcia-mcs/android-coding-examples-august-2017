package com.mcs.practice.to_do.controller;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mcs.practice.to_do.ListFragmentInterface;
import com.mcs.practice.to_do.R;
import com.mcs.practice.to_do.model.ToDo;
import com.mcs.practice.to_do.model.ToDoItemsContact;
import com.mcs.practice.to_do.model.ToDoOpenHelper;
import com.mcs.practice.to_do.networking.ToDoDownloadInterface;
import com.mcs.practice.to_do.view.TodoRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;


public class TodoRecyclerViewFragment extends Fragment implements AdapterView.OnItemClickListener, ToDoDownloadInterface {
	public static final String sKEY_ID = "TODO_ID";
	public static final String sKEY_TODO = "TODO_PARCELABLE";
	public static final int sCREATE_TODO_REQUEST = 333;
	private Context mContext;

	private RecyclerView recyclerView;
	private TodoRecyclerViewAdapter todoListAdapter;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof ListFragmentInterface) {
			// Good, activity implements our interface
			mContext = context;
		} else {
			throw new IllegalArgumentException("Given Context does not implement the ListFragmentInterface");
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_to_do_list, container, false);

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		recyclerView = view.findViewById(R.id.todo_list);
		setToDoListAdapter();
	}

	private void createFakeTask() {
		ToDoOpenHelper todoOpenHelper = new ToDoOpenHelper(getActivity());
		todoOpenHelper.createTodo(new ToDo(-1, "New Task!2", "Can you see me now?", false));
	}

	@Override
	public void onResume() {
		super.onResume();

		// Access SQLite
		//updateToDoList();

		// Consume RESTful API
		setRealTimeUpdate();
	}

	public void updateToDoList() {

		Uri todoUri = ToDoItemsContact.TODO_CONTENT_URI;

		String[] projection = new String[] {
				ToDoOpenHelper.TODO_TABLE_ID,
				ToDoOpenHelper.TODO_TABLE_TITLE,
				ToDoOpenHelper.TODO_TABLE_DETAILS,
				ToDoOpenHelper.TODO_TABLE_COMPLETED
		};

		List<ToDo> toDoList;
		try {
			ContentResolver contentResolver = getActivity().getContentResolver();
			Cursor cursor = contentResolver
					.query(todoUri, projection, null, null, ToDoOpenHelper.TODO_TABLE_ID);
			toDoList = ToDo.parse(cursor);
			todoListAdapter.updateItems(toDoList);
			cursor.close();
		} catch (NullPointerException npe) {
			Log.e(this.getClass().getSimpleName(), npe.getMessage());
		}

	}

	public void setRealTimeUpdate() {
		// Write a message to the database
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference mDatabaseReference = database.getReference("todos");

		Query query = mDatabaseReference;

		final ArrayList<ToDo> toDoArrayList = new ArrayList<>();
		query.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {

				for(DataSnapshot child : dataSnapshot.getChildren()) {
					ToDo toDo = child.getValue(ToDo.class);
					toDoArrayList.add(toDo);
				}
				todoListAdapter.updateItems(toDoArrayList);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {

			}
		});



	}

	private void setToDoListAdapter() {
		todoListAdapter = new TodoRecyclerViewAdapter(getActivity().getApplicationContext(), this);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
		recyclerView.setAdapter(todoListAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		Log.d("TRASH", "onItemClickListener");
		ToDo todo = todoListAdapter.getItem(i);

		((ListFragmentInterface) mContext).onToDoSelected(todo);
	}

	@Override
	public void onTodoResult(List<ToDo> toDoList) {
		todoListAdapter.updateItems(toDoList);
	}

	@Override
	public void onError(Exception exception) {

	}
}

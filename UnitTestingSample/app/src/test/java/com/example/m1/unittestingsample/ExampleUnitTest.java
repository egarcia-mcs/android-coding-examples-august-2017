package com.example.m1.unittestingsample;

import com.example.m1.unittestingsample.utils.Calculator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        // Prepare the test data
        int augend = 5;
        int addend = 8;
        int expectedResult = 13;
        // Do the operation
        long result = Calculator.sum(augend, addend);

        // Verify the result
        assertEquals(expectedResult, result);
    }

    @Test
    public void subtraccion_isCorrect() throws Exception {
        // Prepare the test data
        int minuend = 5;
        int subtrahend = 8;
        int expectedResult = -3;
        // Do the operation
        long result = Calculator.subtract(minuend, subtrahend);

        // Verify the result
        assertEquals(expectedResult, result);
    }

    @Test
    public void multiplication_isCorrect() throws Exception {
        // Prepare the test data
        int multiplicand = 5;
        int multiplier = 8;
        int expectedResult = 40;
        // Do the operation
        long result = Calculator.multiply(multiplicand, multiplier);

        // Verify the result
        assertEquals(expectedResult, result);
    }

    @Test
    public void division_isCorrect() throws Exception {
        // Prepare the test data
        int dividend = 5;
        int divisor = 8;
        int expectedResult = 0;
        // Do the operation
        long result = Calculator.divide(dividend, divisor);

        // Verify the result
        assertEquals(expectedResult, result);
    }
}
package com.mcs.practice.to_do.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mcs.practice.to_do.ListFragmentInterface;
import com.mcs.practice.to_do.networking.NetworkUtils;
import com.mcs.practice.to_do.R;
import com.mcs.practice.to_do.networking.ServerAccessTask;
import com.mcs.practice.to_do.networking.ToDoDownloadInterface;
import com.mcs.practice.to_do.model.ToDo;
import com.mcs.practice.to_do.model.ToDoOpenHelper;
import com.mcs.practice.to_do.view.ToDoListAdapter;

import java.util.List;


public class ToDoListFragment extends Fragment
        implements AdapterView.OnItemClickListener, ToDoDownloadInterface {


    public static final String sKEY_ID = "TODO_ID";
    public static final String sKEY_TODO = "TODO_PARCELABLE";
    public static final int sCREATE_TODO_REQUEST = 333;
    private Context mContext;

    private ListView lv;
    private ToDoListAdapter todoListAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ListFragmentInterface) {
        	// Good, activity implements our interface
	        mContext = context;
        } else {
        	throw new IllegalArgumentException("Given Context does not implement the ListFragmentInterface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_do_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv = view.findViewById(R.id.todo_list);
        setToDoListAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Access SQLite
        //updateToDoList();

        // Consume RESTful API
        fetchToDos();
    }

    private void createFakeTask() {
        ToDoOpenHelper todoOpenHelper = new ToDoOpenHelper(getActivity());
        todoOpenHelper.createTodo(new ToDo(-1, "New Task!2", "Can you see me now?", false));
    }

    public void updateToDoList() {

        //todoOpenHelper.deleteToDo(itemsArray.get(0));
        ToDoOpenHelper todoOpenHelper = new ToDoOpenHelper(getActivity());

        todoOpenHelper.createTodo(new ToDo(-1, "New Task!2", "Can you see me now?", false));
        List<ToDo> itemsArray = todoOpenHelper.getAllTodos();
        todoListAdapter.updateItems(itemsArray);
    }

    /*public void updateApiList(ArrayList<ToDo> todoList) {
        todoListAdapter.updateItems(todoList);
    }*/

    public void fetchToDos() {
        if (NetworkUtils.isOnline(getActivity().getApplicationContext())) {
            ServerAccessTask task = new ServerAccessTask(this);
            task.execute(NetworkUtils.serverAddress);
        }
    }

    private void setToDoListAdapter() {
        todoListAdapter = new ToDoListAdapter(getActivity().getApplicationContext());
        lv.setAdapter(todoListAdapter);
        lv.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("TRASH", "onItemClickListener");
        ToDo todo = todoListAdapter.getItem(i);

	    ((ListFragmentInterface) mContext).onToDoSelected(todo);
    }

    @Override
    public void onTodoResult(List<ToDo> toDoList) {
        todoListAdapter.updateItems(toDoList);
    }

    @Override
    public void onError(Exception exception) {

    }
}

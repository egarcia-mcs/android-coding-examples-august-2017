package com.example.todo.app;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.todo.app.model.ToDo;
import com.example.todo.app.networking.TodoDownloadInterface;

import java.util.List;

public class ToDoDetailsActivity extends AppCompatActivity implements TodoDownloadInterface {
    private EditText titleEditText;
    private EditText contentsEditText;

    // Instance variable that holds the data on the screen
    private ToDo mToDo;
    public static final String TODO_EXTRA = "TODO_EXTRA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_details);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(
                    getResources().getColor(R.color.todo_action_bar)
            );
            actionBar.setBackgroundDrawable(colorDrawable);
        }

        titleEditText = (EditText)  findViewById(R.id.txt_title);
        contentsEditText = (EditText) findViewById(R.id.txt_content);

        // Check if an existing To Do was sent and load it:
        Bundle extras;
        if((extras = getIntent().getExtras()) != null) {
            // Load the To Do contents on the views
            mToDo = (ToDo) extras.get(TODO_EXTRA);
            if (mToDo != null) {
                titleEditText.setText(mToDo.getTitle());
                contentsEditText.setText(mToDo.getContent());
            } else {
                // Handle extras but no to do object sent

            }
        } else {
            // A new To Do is being created
            mToDo = new ToDo(-1, "", "");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.details_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_save:
                // Save action was clicked
                mToDo.setTitle(titleEditText.getText().toString());
                mToDo.setContent(contentsEditText.getText().toString());

                // Send back to the previous activity the data
                Intent intent = new Intent();
                intent.putExtra(TODO_EXTRA, mToDo);
                setResult(RESULT_OK, intent);
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onTodoResult(List<ToDo> toDoList) {

    }

    @Override
    public void onError(Exception exception) {

    }
}

package com.example.todo.app.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ToDoDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    // To Do table name
    public static final String TABLE_TODO = "todos";

    // To Do columns names
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_CONTENT = "content";
    public static final String KEY_COMPLETED = "completed";

    public ToDoDbHelper(Context context) {
        super(context, TABLE_TODO, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sqlCreate = "CREATE TABLE IF NOT EXISTS " + TABLE_TODO + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_TITLE + " TEXT, "
                + KEY_CONTENT + " TEXT, "
                + KEY_COMPLETED + " INTEGER "
                + ")";
        sqLiteDatabase.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // Delete data if there is a database change:
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);

        // Recreate the database
        onCreate(sqLiteDatabase);
    }
}

package com.sample.dagger2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity {

    @Inject
    NetworkApi networkApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView userTextView = (TextView) findViewById(R.id.username);
        final TextView passwordTextView = (TextView) findViewById(R.id.password);
        Button button = (Button) findViewById(R.id.login);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = userTextView.getText().toString();
                String password = passwordTextView.getText().toString();
                if(networkApi.validateUser(username, password)) {
                    Toast.makeText(view.getContext(), "Logged in correctly", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(view.getContext(), "Access denied", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }




}

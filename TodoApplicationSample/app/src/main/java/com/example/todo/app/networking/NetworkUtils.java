package com.example.todo.app.networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {
    public static final String todoServerAddress = "https://jsonplaceholder.typicode.com/todos";
    public static final String SERVER_URL_EXTRA = "server_url_extra";
    public static final String TODO_LIST_EXTRAS = "todo_list_extras";
    public static final String RESULT_RECEIVER_EXTRA = "todo_receiver_extra";
    public static final String RESULT_RECEIVER_ERROR_EXTRA = "todo_receiver_error_extra";

    public static boolean isOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}

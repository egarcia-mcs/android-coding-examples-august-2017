package com.mcs.practice.to_do.model;


import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@IgnoreExtraProperties
public class ToDo implements Parcelable {

    private long mId;
    private String mTitle;
    private String mDetails;
    private boolean mCompleted;

    public ToDo() {
        mId = -1;
        mTitle = "";
        mDetails = "";
        mCompleted = false;
    }

    public ToDo(long mId, String mTitle, String mDetails, boolean mCompleted) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mDetails = mDetails;
        this.mCompleted = mCompleted;
    }

    protected ToDo(Parcel in) {
        mId = in.readLong();
        mTitle = in.readString();
        mDetails = in.readString();
        mCompleted = in.readByte() != 0;
    }

    public static final Creator<ToDo> CREATOR = new Creator<ToDo>() {
        @Override
        public ToDo createFromParcel(Parcel in) {
            return new ToDo(in);
        }

        @Override
        public ToDo[] newArray(int size) {
            return new ToDo[size];
        }
    };

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDetails() {
        return mDetails;
    }

    public void setDetails(String details) {
        mDetails = details;
    }

    public boolean isCompleted() {
        return mCompleted;
    }

    public void setCompleted(boolean completed) {
        mCompleted = completed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(mId);
        parcel.writeString(mTitle);
        parcel.writeString(mDetails);
        parcel.writeByte((byte) (mCompleted ? 1 : 0));
    }

    public static ToDo parse(JSONObject jsonObject) throws JSONException {
        ToDo toDo = new ToDo(
                jsonObject.getInt("id"),
                jsonObject.getString("title"),
                "",
                jsonObject.getBoolean("completed")
        );
        return toDo;
    }

    public static ArrayList<ToDo> parse(JSONArray jsonArray) {
        ArrayList<ToDo> toDos = new ArrayList<>(jsonArray.length() -1);
            try {
                JSONObject jsonObject;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    toDos.add(ToDo.parse(jsonObject));
                }
            } catch (JSONException e) {
                e.printStackTrace();
        }

        return toDos;
    }

    public static ArrayList<ToDo> parse(Cursor cursor) {
        ArrayList<ToDo> todoList = new ArrayList<>();
        while(cursor.moveToNext()) {
            long id = cursor.getLong(0);
            String title = cursor.getString(1);
            String details = cursor.getString(2);
            Boolean completed = cursor.getInt(3) == 0;
            ToDo todo = new ToDo(id, title, details, completed);
            todoList.add(todo);
        }
        return todoList;
    }
}

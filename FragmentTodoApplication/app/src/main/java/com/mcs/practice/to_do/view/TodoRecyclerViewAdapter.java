
package com.mcs.practice.to_do.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.mcs.practice.to_do.R;
import com.mcs.practice.to_do.model.ToDo;

import java.util.ArrayList;
import java.util.List;

public class TodoRecyclerViewAdapter extends RecyclerView.Adapter<TodoRecyclerViewAdapter.TodoViewHolder> {

	private List<ToDo> mToDos;
	private LayoutInflater mLayoutInflater;
	private final AdapterView.OnItemClickListener mOnItemClickListener;

	public TodoRecyclerViewAdapter(Context context, AdapterView.OnItemClickListener listener) {
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mOnItemClickListener = listener;
		mToDos = new ArrayList<>();
	}

	@Override
	public TodoRecyclerViewAdapter.TodoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = mLayoutInflater.inflate(R.layout.custom_row_to_do, null);
		return new TodoViewHolder(view);
	}

	@Override
	public void onBindViewHolder(TodoRecyclerViewAdapter.TodoViewHolder holder, int position) {
		holder.mItem = mToDos.get(position);
		holder.bindListener(mOnItemClickListener);
		holder.mTitle.setText(holder.mItem.getTitle());
	}

	public ToDo getItem(int position) {
		return mToDos.get(position);
	}

	public void updateItems(List<ToDo> items) {
		mToDos = items;
		notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return mToDos.size();
	}

	public class TodoViewHolder extends RecyclerView.ViewHolder {

		public final View mView;
		public final TextView mTitle;
		public ToDo mItem;

		public TodoViewHolder(View itemView) {
			super(itemView);
			mView = itemView;
			mTitle = itemView.findViewById(R.id.title_edit_text);
		}

		public void bindListener(final AdapterView.OnItemClickListener itemClickListener){
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					int position = getAdapterPosition();
					itemClickListener.onItemClick(null, view, position , view.getId());
				}
			});
		}

	}
}

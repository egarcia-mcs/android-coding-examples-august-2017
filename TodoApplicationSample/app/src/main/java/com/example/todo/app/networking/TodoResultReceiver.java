package com.example.todo.app.networking;

import android.os.Bundle;
import android.os.Handler;

import com.example.todo.app.ToDoListActivity;
import com.example.todo.app.model.ToDo;

import java.lang.ref.WeakReference;
import java.util.List;

public class TodoResultReceiver extends android.os.ResultReceiver {
    public static final int RESULT_CODE_OK = 200;
    public static final int RESULT_CODE_ERROR = 400;
    public static WeakReference<TodoDownloadInterface> weakActivity;

    public TodoResultReceiver(Handler handler, TodoDownloadInterface observer) {
        super(handler);
        weakActivity = new WeakReference<TodoDownloadInterface>(observer);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == RESULT_CODE_OK) {
            List<ToDo> toDoList = resultData.getParcelableArrayList(NetworkUtils.TODO_LIST_EXTRAS);

            TodoDownloadInterface observer = (TodoDownloadInterface) weakActivity.get();
            if(observer != null) {
                observer.onTodoResult(toDoList);
            }
        }
    }

}
package bbvacompasschallenge.example.m8s1.bbvacompasschallenge;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import com.example.m8s1.bbvacompasschallenge.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bbvacompasschallenge.example.m8s1.bbvacompasschallenge.views.MapFragment;
import bbvacompasschallenge.example.m8s1.bbvacompasschallenge.views.MapListFragment;

public class MapsActivity extends AppCompatActivity implements NearbyPlacesTask.MapListener {


    private MapListFragment mMapListFragment;
    private MapFragment mMapFragment;
    public static final int REQUEST_LOCATION_CODE = 99;
    private NearbyPlacesTask mNearbyPlacesTask;
    private FusedLocationProviderClient mFusedLocationClient;

    int PROXIMITY_RADIUS = 10000;
    private final String BASE_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json";
    private final String MAPS_KEY = "AIzaSyBGaOD1HmDfa2tFq5RG9Gp61qNTO4an5G4";

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mMapListFragment = new MapListFragment();
        mMapFragment = new MapFragment();

        adapter.addFragment(mMapListFragment, "Nearby Locations");
        adapter.addFragment(mMapFragment, "Map");
        // Attach to the viewpager
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            mNearbyPlacesTask = new NearbyPlacesTask(MapsActivity.this);
                            String hospital = "BBVA+Compass";
                            String url = getUrl(location.getLatitude(), location.getLongitude(), hospital);
                            mNearbyPlacesTask.execute(url);
                        }
                    }
                });

    }

    private String getUrl(double latitude , double longitude , String nearbyPlace) {

        StringBuilder googlePlaceUrl = new StringBuilder(BASE_URL);
        googlePlaceUrl.append("?location=").append(latitude).append(",").append(longitude);
        googlePlaceUrl.append("&radius=").append("10000");
        googlePlaceUrl.append("&type=").append("BBVA+Compass");
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key=").append(MAPS_KEY);

        String googlePlaceUrlString = googlePlaceUrl.toString();
        Log.d("MapsActivity", "url = " + googlePlaceUrlString);
        return googlePlaceUrlString;
    }




    public boolean checkLocationPermission()
    {
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)  != PackageManager.PERMISSION_GRANTED )
        {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION },REQUEST_LOCATION_CODE);
            }
            else
            {
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION },REQUEST_LOCATION_CODE);
            }
            return false;

        }
        else
            return true;
    }

    public void onMapsResult(List<HashMap<String, String>> nearbyPlaceList) {
        mMapFragment.setNearbyLocations(nearbyPlaceList);
        mMapListFragment.setDataset(nearbyPlaceList);
    }

}
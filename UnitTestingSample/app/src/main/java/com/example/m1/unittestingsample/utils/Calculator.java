package com.example.m1.unittestingsample.utils;

public class Calculator {
    // 3 + 6 = 9
    public static long sum(int augend, int addend) {
        return augend + addend;
    }

    public static long subtract(int minuend, int subtrahend) {
        return minuend - subtrahend;
    }

    public static long multiply(int multiplicand, int multiplier) {
        return multiplicand * multiplier;
    }

    public static long divide(int dividend, int divisor) {
        return dividend / divisor;
    }
}

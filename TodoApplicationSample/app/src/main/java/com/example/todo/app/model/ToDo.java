package com.example.todo.app.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ToDo implements Parcelable {
    private long id;
    private String title;
    private String content;
    private boolean completed;

    public ToDo(long id, String title, String content, boolean completed) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.completed = completed;

        // Log when a new To Do item is created
        Log.d(this.getClass().getSimpleName(), "New todo created: " + this.toString());
    }

    public ToDo(long id, String title, String content) {
        this(id, title, content, false);
    }

    protected ToDo(Parcel in) {
        id = in.readLong();
        title = in.readString();
        content = in.readString();
        completed = in.readByte() != 0;
    }

    public static final Creator<ToDo> CREATOR = new Creator<ToDo>() {
        @Override
        public ToDo createFromParcel(Parcel in) {
            return new ToDo(in);
        }

        @Override
        public ToDo[] newArray(int size) {
            return new ToDo[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ToDo
                && ((ToDo) o).getId() == this.getId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(content);
        parcel.writeByte((byte) (completed ? 1 : 0));
    }

    public void save(Context context){
        ToDoDbHelper toDoDbHelper = new ToDoDbHelper(context.getApplicationContext());
        SQLiteDatabase sqLiteDatabase = toDoDbHelper.getWritableDatabase();
        ContentValues todoValues = new ContentValues();

        todoValues.put(ToDoDbHelper.KEY_TITLE, title);
        todoValues.put(ToDoDbHelper.KEY_CONTENT, content);
        todoValues.put(ToDoDbHelper.KEY_COMPLETED, completed ? 1 : 0);
        long itemId;
        if (id != -1) { // Update a To Do
            todoValues.put(ToDoDbHelper.KEY_ID, id);
            itemId = sqLiteDatabase.update(
                    ToDoDbHelper.TABLE_TODO, todoValues,
                    ToDoDbHelper.KEY_ID + " = ?",
                    new String[] { String.valueOf(id)}
            );
        } else { // Create a To Do
            itemId = sqLiteDatabase.insertOrThrow(ToDoDbHelper.TABLE_TODO, null, todoValues);
            this.id = itemId;
        }
        sqLiteDatabase.close();
    }

    public void delete(Context context) {
        ToDoDbHelper todoDbHelper = new ToDoDbHelper(context);
        SQLiteDatabase sqliteDatabase = todoDbHelper.getWritableDatabase();

        int result = sqliteDatabase.delete(
                ToDoDbHelper.TABLE_TODO,
                ToDoDbHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
    }

    public static List<ToDo> getToDos(Context context) {
        ArrayList<ToDo> todos = new ArrayList<>();
        ToDoDbHelper toDoDbHelper = new ToDoDbHelper(context);
        // Query for all to do's
        String selectAllQuery = "Select * FROM " + ToDoDbHelper.TABLE_TODO;

        SQLiteDatabase sqLiteDatabase = toDoDbHelper.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectAllQuery, null);

        while (cursor.moveToNext()) {
            ToDo todo = new ToDo(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3) == 1);
            todos.add(todo);
        }
        cursor.close();
        return todos;
    }

    public static ArrayList<ToDo> parseJSONArray(JSONArray jsonArray){
        ArrayList<ToDo> todosList = new ArrayList<>(jsonArray.length() + 1);
        try {
            JSONObject jsonObject;
            for (int index = 0; index < jsonArray.length(); index++) {
                jsonObject = jsonArray.getJSONObject(index);
                ToDo todo = new ToDo(
                        jsonObject.getInt("id"),
                        jsonObject.getString("title"),
                        "");
                todosList.add(todo);
            }
        } catch (JSONException exception) {
            Log.d(ToDo.class.getSimpleName(), exception.getLocalizedMessage());
        }
        return todosList;
    }
}

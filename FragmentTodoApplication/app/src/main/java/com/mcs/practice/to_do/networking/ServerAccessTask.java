package com.mcs.practice.to_do.networking;

import android.os.AsyncTask;

import com.mcs.practice.to_do.model.ToDo;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class ServerAccessTask extends AsyncTask<String, Void, List<ToDo>> {

    // ProgressDialog to show while performing
    //private ProgressDialog PD;

    private WeakReference<ToDoDownloadInterface> mObserverWeakReference;

    public ServerAccessTask(ToDoDownloadInterface downloadInterface) {
        mObserverWeakReference = new WeakReference<>(downloadInterface);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Showing progress dialog while accessing server
        //PD = ProgressDialog.show(context, "Accessing Server", "Please wait...", false, false);
    }

    @Override
    protected List<ToDo> doInBackground(String... strings) {
        ArrayList<ToDo> toDos = null;
        try {
            URL url = new URL(strings[0]);
            // Check if we have internet connection
            // TODO: Check connectivity
            //Start the connection
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream)
            );

            StringBuilder stringBuilder = new StringBuilder();
            String inputString;

            while ( (inputString = bufferedReader.readLine()) != null) {
                stringBuilder.append(inputString);
            }

            JSONArray jsonArray = new JSONArray(stringBuilder.toString());
            toDos = ToDo.parse(jsonArray);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return toDos;
    }

    @Override
    protected void onPostExecute(List<ToDo> toDos) {
        ToDoDownloadInterface activity = mObserverWeakReference.get();
        if (activity != null) {
            activity.onTodoResult(toDos);
        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
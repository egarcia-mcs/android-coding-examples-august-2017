package com.example.todo.app.networking;

import android.os.AsyncTask;
import android.util.Log;

import com.example.todo.app.model.ToDo;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;

import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class DownloadTodosTask extends AsyncTask <String, Void, List<ToDo>> {

    private WeakReference<TodoDownloadInterface> observerWeakReference;

    public DownloadTodosTask(TodoDownloadInterface downloadTaskInterface) {
        observerWeakReference = new WeakReference<TodoDownloadInterface>(downloadTaskInterface);
    }

    @Override
    protected List<ToDo> doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream)
            );

            StringBuilder stringBuilder = new StringBuilder();
            String inputString;

            while((inputString = bufferedReader.readLine()) != null) {
                stringBuilder.append(inputString);
            }

            JSONArray jsonArray = new JSONArray(stringBuilder.toString());
            List<ToDo> todosList = ToDo.parseJSONArray(jsonArray);
            connection.disconnect();
            return todosList;

        } catch (Exception exception) {
            // Manage exception
            Log.d(this.getClass().getSimpleName(), exception.getLocalizedMessage());
        }
        // No data or an exception happened.
        return null;
    }

    @Override
    protected void onPostExecute(List<ToDo> toDoList) {
        TodoDownloadInterface observer = observerWeakReference.get();
        if(observer != null) {
            observer.onTodoResult(toDoList);
        }
    }
}

package com.mcs.practice.to_do.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class ToDoOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "TODO_DATABASE";
    public static final String TODO_TABLE = "TODOS";
    public static final String TODO_TABLE_ID = "ID";
    public static final String TODO_TABLE_TITLE = "TITLE";
    public static final String TODO_TABLE_DETAILS = "DETAILS";
    public static final String TODO_TABLE_COMPLETED = "COMPLETED";

    public ToDoOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TODO_TABLE = "CREATE TABLE " + TODO_TABLE + " ("
                + TODO_TABLE_ID + " INTEGER PRIMARY KEY, "
                + TODO_TABLE_TITLE + " TEXT, "
                + TODO_TABLE_DETAILS + " TEXT, "
                + TODO_TABLE_COMPLETED + " INTEGER" + ")";
        sqLiteDatabase.execSQL(CREATE_TODO_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TODO_TABLE);
        onCreate(sqLiteDatabase);
    }

    public long createTodo(ToDo todo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TODO_TABLE_TITLE, todo.getTitle());
        values.put(TODO_TABLE_DETAILS, todo.getDetails());
        long todoId = db.insert(TODO_TABLE, null, values);

        db.close();
        return todoId;
    }

    public long insert(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        long todoId = db.insert(TODO_TABLE, null, contentValues);
        db.close();
        return todoId;
    }

    public void updateToDo(ToDo todo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues CV = new ContentValues();
        CV.put(TODO_TABLE_TITLE, todo.getTitle());
        CV.put(TODO_TABLE_DETAILS, todo.getDetails());
        CV.put(TODO_TABLE_COMPLETED, todo.isCompleted());

        String id = String.valueOf(todo.getId());
        db.update(TODO_TABLE, CV,
                TODO_TABLE_ID + " = ?", new String[] {id});
    }

    public void deleteToDo(ToDo todo) {
        String id = String.valueOf(todo.getId());
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TODO_TABLE, TODO_TABLE_ID + " = ?", new String[] {id});
        db.close();
    }

    public List<ToDo> getAllTodos() {
        List<ToDo> todoList;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        String query = "Select * from table " + TODO_TABLE;
        cursor = db.rawQuery(query, null);

        todoList = ToDo.parse(cursor);

        cursor.close();

        return todoList;
    }

    public Cursor query(String table, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
    	SQLiteDatabase db = this.getReadableDatabase();
    	return db.query(TODO_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
    }
}

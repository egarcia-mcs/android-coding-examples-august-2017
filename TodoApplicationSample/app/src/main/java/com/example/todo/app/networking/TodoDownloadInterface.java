package com.example.todo.app.networking;

import com.example.todo.app.model.ToDo;

import java.util.List;

public interface TodoDownloadInterface {
    void onTodoResult(List<ToDo> toDoList);
    void onError(Exception exception);
}



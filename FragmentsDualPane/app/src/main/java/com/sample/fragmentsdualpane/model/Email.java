package com.sample.fragmentsdualpane.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Email implements Parcelable {
    private String header;
    private String content;

    public Email(String header, String content) {
        this.header = header;
        this.content = content;
    }

    protected Email(Parcel in) {
        header = in.readString();
        content = in.readString();
    }

    public static final Creator<Email> CREATOR = new Creator<Email>() {
        @Override
        public Email createFromParcel(Parcel in) {
            return new Email(in);
        }

        @Override
        public Email[] newArray(int size) {
            return new Email[size];
        }
    };

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(header);
        parcel.writeString(content);
    }
}

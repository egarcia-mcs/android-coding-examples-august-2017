package com.example.todo.app.model;

public class TodoRequest {
    public static final int NEW = 0;
    public static final int EDIT = 1;
    public static final int DELETE = 2;
}

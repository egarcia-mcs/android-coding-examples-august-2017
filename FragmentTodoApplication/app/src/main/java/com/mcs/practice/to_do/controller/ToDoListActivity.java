package com.mcs.practice.to_do.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.mcs.practice.to_do.ListFragmentInterface;
import com.mcs.practice.to_do.R;
import com.mcs.practice.to_do.SingleFragmentActivity;
import com.mcs.practice.to_do.model.ToDo;


public class ToDoListActivity extends SingleFragmentActivity implements ListFragmentInterface {

	private boolean dualPane;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Check if we are in dual pane mode:
		dualPane = findViewById(R.id.details_container) != null;
		// TODO: Load the first item on the list and load the details fragment

		FloatingActionButton newTodoFab = findViewById(R.id.fab_add_todo);
		newTodoFab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(ToDoListActivity.this, ToDoDetailsActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
    protected Fragment createFragment() {
        return new TodoRecyclerViewFragment();
    }

    @Override
    public void onToDoSelected(ToDo toDo) {
        FragmentManager fm = getSupportFragmentManager();

        Fragment detailsFragment = TodoDetailsFragment.newInstance(toDo);

        if(dualPane) {
        	// Big screen device dual fragment mode
	        fm.beginTransaction()
			        .replace(R.id.details_container, detailsFragment)
			        .commit();
        } else {
        	// Single fragment mode
	        fm.beginTransaction()
			        .replace(R.id.fragment_container, detailsFragment)
			        .addToBackStack("Details")
			        .commit();
        }

    }


}

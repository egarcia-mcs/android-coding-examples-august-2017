package com.example.todo.app.view;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.todo.app.R;
import com.example.todo.app.model.ToDo;

import java.util.List;


public class TodoRecyclerViewAdapter  extends RecyclerView.Adapter<TodoRecyclerViewAdapter.ViewHolder> {
    private List<ToDo> mItems;
    private Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    public TodoRecyclerViewAdapter(Context context, List<ToDo> items, AdapterView.OnItemClickListener listener){
        this.onItemClickListener = listener;
        this.mItems = items;
        this.mContext = context;
    }

    /**
     * Method that given a list of To Do items will replace the current data on the adapter
     * and refresh the RecyclerView contents
     * @param items the todo list of items to display on the recyclerview
     */
    public void setDataset(List<ToDo> items){
        mItems = items;
        this.notifyDataSetChanged();
    }

    @Override
    public TodoRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.todo_list_item, parent, false);
        TodoRecyclerViewAdapter.ViewHolder viewHolder =
                new TodoRecyclerViewAdapter.ViewHolder(view, onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TodoRecyclerViewAdapter.ViewHolder holder, int position) {
        ToDo toDo = mItems.get(position);
        holder.textTitle.setText(toDo.getTitle());
        if (toDo.isCompleted()) {
            holder.imageStatus.setImageResource(R.mipmap.ic_done_black_24dp);
            holder.textTitle.setPaintFlags(
                    holder.textTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG
            );
        } else {
            // No image
            holder.imageStatus.setImageResource(android.R.color.transparent);
            holder.textTitle.setPaintFlags(
                    holder.textTitle.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG)
            );
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public ToDo getItem(int index) {
        return mItems.get(index);
    }

    public void addItem(ToDo toDo){
        toDo.save(mContext);
        updateItems();
    }
    public void deleteItem(ToDo todo) {
        todo.delete(mContext);
        updateItems();
    }

    public void updateItems() {
        mItems = ToDo.getToDos(mContext);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView textTitle;
        private ImageView imageStatus;
        private TextView textContent;
        private AdapterView.OnItemClickListener onItemClickListener;

        public ViewHolder(View itemView, AdapterView.OnItemClickListener listener) {
            super(itemView);
            textTitle = (TextView) itemView.findViewById(R.id.lbl_todo_title);
            imageStatus = (ImageView) itemView.findViewById(R.id.image_todo_item);
            textContent = (TextView) itemView.findViewById(R.id.lbl_todo_contents);
            onItemClickListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());
        }
    }
}

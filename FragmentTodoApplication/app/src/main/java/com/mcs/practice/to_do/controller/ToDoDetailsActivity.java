package com.mcs.practice.to_do.controller;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mcs.practice.to_do.R;
import com.mcs.practice.to_do.model.ToDo;
import com.mcs.practice.to_do.model.ToDoItemsContact;
import com.mcs.practice.to_do.model.ToDoOpenHelper;

public class ToDoDetailsActivity extends AppCompatActivity {

    TextView mTitle, mDetails;
    ToDo mToDo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_details);

        mTitle = findViewById(R.id.title_edit_text);
        mDetails = findViewById(R.id.details_edit_text);

        Intent intent = getIntent();

        mToDo = intent.getParcelableExtra(ToDoListFragment.sKEY_TODO);
        if(mToDo != null) {
            // Edit mode
            loadToDo();
        } else {
        	// Create to do
            mToDo = new ToDo();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save:
                saveToDo();
                break;
            default:
                Log.d(this.getLocalClassName(), "Unrecognized menu item");
        }
        return true;
    }

    private void loadToDo() {
        mTitle.setText(mToDo.getTitle());
        mDetails.setText(mToDo.getDetails());
    }

    private void saveToDo() {
        mToDo.setTitle(mTitle.getText().toString());
        mToDo.setDetails(mDetails.getText().toString());

	    // Write a message to the database
	    FirebaseDatabase database = FirebaseDatabase.getInstance();
	    DatabaseReference myRef = database.getReference("todos");

	    String todoID = myRef.push().getKey();

	    myRef.child(todoID).setValue(mToDo);

	    setResult(RESULT_OK);
        finish();
    }
}

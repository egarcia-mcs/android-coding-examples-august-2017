package com.sample.contactssample.presenter;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.sample.contactssample.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactInteractor {
    public static List<Contact> fetchContacts(Context context) {
        List<Contact> contactsList = new ArrayList<>();

        Uri CONTACTS_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;

        String NAME = ContactsContract.Contacts.DISPLAY_NAME;

        String HAS_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PHONE_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String PHONE_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        Uri EMAIL_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EMAIL_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String EMAIL = ContactsContract.CommonDataKinds.Email.DATA;

        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(CONTACTS_URI, null, null, null, null);

        int count = cursor.getCount();
        for(int index = 0; index < count; index++) {
            cursor.moveToNext();
            String contactID = cursor.getString(cursor.getColumnIndex(_ID));
            String name = cursor.getString(cursor.getColumnIndex(NAME));

            // Get the first phone number:
            String phoneNumber = "";
            int how_many_numbers = Integer.parseInt(
                    cursor.getString(cursor.getColumnIndex(HAS_NUMBER)));
            if(how_many_numbers > 0) {
                Cursor phoneCursor = contentResolver.query(
                        PHONE_URI,
                        null, PHONE_ID + " = ?", new String[] {contactID}, null);
                phoneCursor.moveToNext();
                phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                phoneCursor.close();
            }
            // Get the first email

            Cursor emailCursor = contentResolver.query(
                    EMAIL_URI, null, EMAIL_ID + " = ?", new String[]{contactID}, null);

            String email = "";
            if (emailCursor != null && emailCursor.moveToNext()) {
                email = emailCursor.getString(emailCursor.getColumnIndex(EMAIL));
            }

            Contact contact = new Contact(name, phoneNumber, email);
            contactsList.add(contact);
            Log.d(ContactInteractor.class.getSimpleName(), "Contact retrieved.");
        }
        cursor.close();
        return contactsList;
    }
}

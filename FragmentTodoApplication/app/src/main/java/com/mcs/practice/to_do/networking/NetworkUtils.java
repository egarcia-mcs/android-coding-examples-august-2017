package com.mcs.practice.to_do.networking;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {
    public static final String serverAddress = "https://jsonplaceholder.typicode.com/todos";
    public static final String SERVER_URL_EXTRA = "server_url_extra";
    public static final String TODO_LIST_EXTRA = "todo_list_extra";
    public static final String RESULT_RECEIVER_EXTRA = "todo_receiver_extra";
    public static final String RESULT_RECEIVER_ERROR_EXTRA = "todo_receiver_error_extra";

    public static boolean isOnline(Context context) {
        ConnectivityManager manager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());
    }
}

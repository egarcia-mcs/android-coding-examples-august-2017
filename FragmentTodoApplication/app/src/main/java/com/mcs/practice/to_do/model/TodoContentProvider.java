package com.mcs.practice.to_do.model;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class TodoContentProvider extends ContentProvider {

	private ToDoOpenHelper sqLiteOpenHelper;



	static final UriMatcher uriMatcher;
	static final int TODOS_RESOURCE_CODE = 1;
	static final int TODO_ITEM_RESOURCE_CODE = 2;

	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(ToDoItemsContact.AUTHORITY, ToDoItemsContact.TODO_RESOURCE, TODOS_RESOURCE_CODE);
		uriMatcher.addURI(ToDoItemsContact.AUTHORITY, ToDoItemsContact.TODO_RESOURCE + "/#", TODO_ITEM_RESOURCE_CODE);
	}

	public TodoContentProvider() {	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// Implement this to handle requests to delete one or more rows.
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public String getType(Uri uri) {
		// TODO: Implement this to handle requests for the MIME type of the data
		// at the given URI.
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long todoId = sqLiteOpenHelper.insert(values);
		Uri todoUri = ContentUris.withAppendedId(ToDoItemsContact.TODO_CONTENT_URI, todoId);

		// Write a message to the database
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference myRef = database.getReference("todos");

		String userId = myRef.push().getKey();

		ToDo toDo = new ToDo(
				-1,
				values.getAsString(ToDoItemsContact.TODO_TABLE_TITLE),
				values.getAsString(ToDoItemsContact.TODO_TABLE_DETAILS),
				values.getAsBoolean(ToDoItemsContact.TODO_TABLE_COMPLETED));


		getContext().getContentResolver().notifyChange(todoUri, null);
		return todoUri;
	}

	@Override
	public boolean onCreate() {
		sqLiteOpenHelper = new ToDoOpenHelper(getContext());
		return sqLiteOpenHelper != null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
	                    String[] selectionArgs, String sortOrder) {
		Cursor cursor = sqLiteOpenHelper.query(ToDoOpenHelper.TODO_TABLE, projection, selection, selectionArgs, sortOrder);
		return cursor;
	}

	public int update(Uri uri, ContentValues values, String selection,
	                  String[] selectionArgs) {
		// TODO: Implement this to handle requests to update one or more rows.
		throw new UnsupportedOperationException("Not yet implemented");
	}
}

package com.mcs.practice.to_do.model;

import android.net.Uri;

public final class ToDoItemsContact {

	public static final String TODO_RESOURCE = "todos";
	public static final String AUTHORITY = "sampletodoapp";
	public static final Uri TODO_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TODO_RESOURCE);

	public static final String TODO_TABLE = "TODOS";
	public static final String TODO_TABLE_ID = "ID";
	public static final String TODO_TABLE_TITLE = "TITLE";
	public static final String TODO_TABLE_DETAILS = "DETAILS";
	public static final String TODO_TABLE_COMPLETED = "COMPLETED";

	//public static final class ToDos implements

}

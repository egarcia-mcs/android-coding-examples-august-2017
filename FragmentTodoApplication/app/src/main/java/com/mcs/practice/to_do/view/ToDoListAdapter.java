package com.mcs.practice.to_do.view;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mcs.practice.to_do.R;
import com.mcs.practice.to_do.model.ToDo;

import java.util.ArrayList;
import java.util.List;


public class ToDoListAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;

    private List<ToDo> mToDos;

    public ToDoListAdapter(Context context) {
        this.mLayoutInflater =
                (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mToDos = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mToDos.size();
    }

    @Override
    public ToDo getItem(int i) {
        return mToDos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mToDos.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = mLayoutInflater.inflate(R.layout.custom_row_to_do, null);

        TextView mTitle = view.findViewById(R.id.title_edit_text);
        //CheckBox mCheckbox = view.findViewById(R.id.completion_checkbox);

        ToDo todo = mToDos.get(i);
        mTitle.setText(todo.getTitle());
        //mCheckbox.setChecked(todo.isCompleted());

        return view;
    }

    public void updateItems(List<ToDo> items) {
        this.mToDos = items;
        notifyDataSetChanged();
    }
}

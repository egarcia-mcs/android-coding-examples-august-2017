package com.example.m1.weatherapplicationsample;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m1.weatherapplicationsample.View.WeatherView;

public class WeatherRecyclerViewAdapter extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.WeatherViewHolder> {
    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.weather_card_item, parent, false);
        WeatherViewHolder weatherViewHolder = new WeatherViewHolder(view);
        return weatherViewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        // Set texts
        holder.header.setText("Today");
        holder.weatherView1.hourText.setText("4:00 PM");
        holder.weatherView1.imageForecast.setImageResource(R.mipmap.ic_launcher);
        holder.weatherView1.temperatureText.setText("8*");
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public static class WeatherViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView header;
        WeatherView weatherView1;
        WeatherView weatherView2;
        WeatherView weatherView3;


        public WeatherViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.weather_card_view_item);
            header =  itemView.findViewById(R.id.header);
            weatherView1 = itemView.findViewById(R.id.weather_item_1);
            weatherView2 = itemView.findViewById(R.id.weather_item_2);
            weatherView3 = itemView.findViewById(R.id.weather_item_3);
        }
    }
}

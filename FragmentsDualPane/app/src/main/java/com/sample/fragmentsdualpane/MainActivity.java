package com.sample.fragmentsdualpane;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import com.sample.fragmentsdualpane.model.Email;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
   // Variable to check if we are displaying one or two fragments at the same time
    private boolean mDualPane;
    private EmailListAdapter mEmailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Email> emails = new ArrayList<>(10);
        emails.add(new Email(
                "Bring your parents to work day",
                "Hey! what do you thing about a barbecue on the evening, then going to the movies"));
        emails.add(new Email(
                "Picture for last Saturday",
                "Check out the new friend we made, the dog afterwards was very friendly and kind."));
        emails.add(new Email(
                "Board game night?",
                "Sunday works! If you can get Dexter to come, the better, as he is very fierce with everybody."));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.email_list);

        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Email email = mEmailAdapter.getItem(i);
                Intent intent = new Intent(MainActivity.this, EmailDetailsActivity.class);
                intent.putExtra(EmailDetailsActivity.EMAIL_EXTRA, email);
                startActivity(intent);
            }
        };
        mEmailAdapter = new EmailListAdapter(emails, listener);
        recyclerView.setAdapter(mEmailAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Check if the FrameLayout was loaded into memory (inflated)
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_email_details);
        // mDualPane will be true if the FrameLayout was loaded into memory, aka not null
        mDualPane = frameLayout != null;

        if (mDualPane) {
            // Initialize and load the fragment into the frame layout
            EmailDetailsFragment emailDetailsFragment =
                    EmailDetailsFragment.newInstance(emails.get(0));
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_email_details, emailDetailsFragment);
            fragmentTransaction.commit();
        }
    }
}

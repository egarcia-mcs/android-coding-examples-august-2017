package com.sample.dagger2;

import javax.inject.Inject;

public class NetworkApi {

    @Inject
    public NetworkApi(){}

    public boolean validateUser(String username, String password) {
        // Call RESTful server here and handle asynchronously
        // As of demo, just validate if its not empty

        return !username.isEmpty() && !password.isEmpty();
    }
}

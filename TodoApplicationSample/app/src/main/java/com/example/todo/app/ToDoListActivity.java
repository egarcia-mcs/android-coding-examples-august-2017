package com.example.todo.app;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.todo.app.model.ToDo;
import com.example.todo.app.model.TodoRequest;
import com.example.todo.app.networking.DownloadTodosService;
import com.example.todo.app.networking.DownloadTodosTask;
import com.example.todo.app.networking.NetworkUtils;
import com.example.todo.app.networking.TodoResultReceiver;
import com.example.todo.app.networking.TodoDownloadInterface;
import com.example.todo.app.view.TodoRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class ToDoListActivity extends AppCompatActivity implements TodoDownloadInterface {

    private TodoRecyclerViewAdapter todoAdapter;
    private RecyclerView recyclerView;
    private DownloadTodosTask downloadTodosTask;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(
                    getResources().getColor(R.color.todo_action_bar)
            );
            actionBar.setBackgroundDrawable(colorDrawable);
        }

        // Retrieving the ListView object:
        recyclerView = (RecyclerView) findViewById(R.id.list_todos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // Set the ListView adapter
        setAdapter();
        // Set the required listeners
        setListeners();
    }

    private void setListeners() {

        // Set the To Do status as completed
        /*
        recyclerView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                ToDo toDo = (ToDo) todoAdapter.getItem(i);
                todoAdapter.deleteItem(toDo);
                return false;
            }
        });
        */

        // Clear the completed To Dos
        Button todoDeleteButton = (Button) findViewById(R.id.btn_todo_delete);
        todoDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todoAdapter.clearCompleted();
            }
        });

        FloatingActionButton todoAddButton = (FloatingActionButton) findViewById(R.id.btn_todo_add);
        todoAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ToDoListActivity.this, ToDoDetailsActivity.class);
                startActivityForResult(intent, TodoRequest.NEW);
            }
        });
    }

    private void setAdapter(){
        // Show the To Do item details
        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Getting the item object from the adapter at the index position
                ToDo todoItem = (ToDo) todoAdapter.getItem(i);

                Intent intent = new Intent(ToDoListActivity.this, ToDoDetailsActivity.class);
                // Include the To Do item to display on the details activity as extra
                intent.putExtra(ToDoDetailsActivity.TODO_EXTRA, todoItem);

                // Start the To Do Details Activity
                startActivityForResult(intent, TodoRequest.EDIT);
            }
        };

        List <ToDo> items = fetchData();
        todoAdapter = new TodoRecyclerViewAdapter(getApplicationContext(), items, listener);
        // Set the adapter to the ListView
        recyclerView.setAdapter(todoAdapter);
    }

    private List<ToDo> fetchData(){

        List<ToDo> todos = new ArrayList<ToDo>();
        // Check for connectivity
        if (NetworkUtils.isOnline(getApplicationContext())){
            // Get the data from the RESTful service

            Intent intent = new Intent(this, DownloadTodosService.class);
            intent.putExtra(NetworkUtils.SERVER_URL_EXTRA, NetworkUtils.todoServerAddress);
            TodoResultReceiver todoResultReceiver = new TodoResultReceiver(
                    new Handler(this.getMainLooper()), this);
            intent.putExtra(NetworkUtils.RESULT_RECEIVER_EXTRA, todoResultReceiver);
            this.startService(intent);
            //downloadTodosTask = new DownloadTodosTask(this);
            //downloadTodosTask.execute(NetworkUtils.todoServerAddress);

        } else {
            // Load from the database
            todos = ToDo.getToDos(this);
            progressBar.setVisibility(View.GONE);

        }

        return todos;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            ToDo toDo = data.getParcelableExtra(ToDoDetailsActivity.TODO_EXTRA);
            switch(requestCode) {
                case TodoRequest.NEW:
                case TodoRequest.EDIT:
                    todoAdapter.addItem(toDo);
                    break;
                case TodoRequest.DELETE:
                    todoAdapter.deleteItem(toDo);
                    break;
                default:
                    break;
            }
        } else {
            Log.d(this.getClass().getSimpleName(), "ToDo activity result action cancelled or failed");
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        // Cancel any data refreshing operation
        if(downloadTodosTask != null && !downloadTodosTask.isCancelled()) {
            downloadTodosTask.cancel(true);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Refresh the todo data
        fetchData();
    }

    @Override
    public void onTodoResult(List<ToDo> toDoList) {
        progressBar.setVisibility(View.GONE);
        // Get the information from the network
        todoAdapter.setDataset(toDoList);
    }

    @Override
    public void onError(Exception exception) {
        // Could not retrieve todos from the restful service
    }


}

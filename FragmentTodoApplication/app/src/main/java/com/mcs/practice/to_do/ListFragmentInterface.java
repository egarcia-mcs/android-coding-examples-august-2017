package com.mcs.practice.to_do;


import com.mcs.practice.to_do.model.ToDo;

public interface ListFragmentInterface {

    public void onToDoSelected(ToDo toDo);

}
